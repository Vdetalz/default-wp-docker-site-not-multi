<?php
/**
 * Air Kalium functions, enqueues parent and child stylesheets by default.
 *
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

if ( ! function_exists( 'air_kalium_theme_styles' ) ) {
  /**
   * Add parent theme styles.
   */
  function air_kalium_theme_styles() {
    wp_enqueue_style('parent-kalium-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('air-kalium-style', get_stylesheet_directory_uri() . '/style.css', ['parent-kalium-style']);
  }

  add_action('wp_enqueue_scripts', 'air_kalium_theme_styles');
}
