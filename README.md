Requirements
------------

* Serveur Apache
* PHP 7.x
* Mysql 5.6
* Composer
* Docker (for the development)

Use
-----

#### Cron

Nothing yet.


#### CLI

Nothing yet.


#### Running Tests

Nothing yet.


Installation
------------
This version is based on Docker and can run in local without other dependencies.

1. Install Docker

2. Clone the project locally
```
mkdir project-name
cd project-name
git clone git@github.com:kernix/project-name.git
```

3. Add to your host
```
127.0.0.1       project-name.local
```

4. Launch containers beforehand
```
docker-compose up -d
```

5. Change the name of `wp-config.dist.php`
```
mv app/wp-config.dist.php app/wp-config.php
```

6. Recover the database from host

On the back office of wordpress from host

Outil -> Migrate DB

```
//project-name.local:8000
/var/web/app

Then change your database
PhpMyAdmin accessible : `http://project-name.local:8001/`

```

7. Recover folder `upload` by FTP from host


8. Access to the site on your browser locally
```
Open the browser : `http://project-name.local:8000/`

PhpMyAdmin accessible : `http://project-name.local:8001/`
```

Documentation
-------------
### Docker commands
### Launch of the containers
```
docker-compose up -d
```

### Connection to docker
```
docker-compose exec apache bash
```

### Stopping the containers
```
docker-compose stop
```
